########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

from qops_tablet import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime,
                           default=db.func.now(),
                           onupdate=db.func.now())


class Location(Base):
    __tablename__ = 'location'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    description = db.Column(db.String)
    kind_id = db.Column(db.Integer,
                        db.ForeignKey('location_kind.id'))
    usage = db.Column(db.String)


class LocationKind(Base):
    __tablename__ = 'location_kind'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class LocationAddressKind(Base):
    __tablename__ = 'location_address_kind'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    usage = db.Column(db.String)


class LocationAddress(Base):
    __tablename__ = 'location_address'

    identifier = db.Column(db.String(25))
    kind_id = db.Column(db.Integer,
                        db.ForeignKey('location_address_kind.id'))
    city = db.Column(db.String)
    state = db.Column(db.String)
    zip_code = db.Column(db.String)
    usage = db.Column(db.String)


class LocationAddressLine(Base):
    __tablename__ = 'location_address_line'

    location_address_id = db.Column(db.Integer,
                                    db.ForeignKey('location_address.id'))
    address_line = db.Column(db.String)
    sequence = db.Column(db.Integer)
