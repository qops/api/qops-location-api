########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

# Import models
from .models import Location
from .models import LocationKind

# Import forms
from .forms import LocationProfileForm

import logging
logger = logging.getLogger('qops')


# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_location = Blueprint('location',
                         __name__,
                         url_prefix='/location')


@mod_location.context_processor
def store():
    store_dict = {'serviceName': 'Location',
                  'serviceDashboardUrl': url_for('location.dashboard'),
                  'serviceBrowseUrl': url_for('location.browse'),
                  'serviceNewUrl': url_for('location.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_location.route('/', methods=['GET'])
def location():
    return render_template('location/location_dashboard.html')


@mod_location.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('location/location_dashboard.html')


@mod_location.route('/browse', methods=['GET'])
def browse():
    locations = Location.query.all()
    return render_template('location/location_browse.html',
                           locations=locations)


@mod_location.route('/new', methods=['GET', 'POST'])
def new():
    location = Location()
    form = LocationProfileForm(request.form)
    form.kind_id.choices = [(k.id, k.name) for k in LocationKind.query.all()]

    if request.method == 'POST':
        logger.debug('IN LOCATION.NEW(POST)')
        logger.debug('Location Identifier : {0}'.format(form.identifier.data))
        logger.debug('Location Name       : {0}'.format(form.name.data))
        logger.debug('Location Description: {0}'.format(form.description.data))
        logger.debug('Location Kind       : {0}'.format(form.kind_id.data))
        form.populate_obj(location)
        location.identifier = Location.get_next_identifier()
        db.session.add(location)
        db.session.commit()

        return redirect(url_for('location.browse'))
    return render_template('location/location_new.html',
                           location=location,
                           form=form)


@mod_location.route('/profile', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/profile',
                    methods=['GET', 'POST'])
def profile(location_id=None):
    location = Location.query.get(location_id)
    form = LocationProfileForm(obj=location)

    form.kind_id.choices = [(k.id, k.name) for k in LocationKind.query.all()]

    if request.method == 'POST':
        logger.debug('IN LOCATION.PROFILE(POST)')
        logger.debug('Location Identifier : {0}'.format(form.identifier.data))
        logger.debug('Location Name       : {0}'.format(form.name.data))
        logger.debug('Location Description: {0}'.format(form.description.data))
        logger.debug('Location Kind       : {0}'.format(form.kind_id.data))
        form = LocationProfileForm(request.form)
        form.populate_obj(location)
        db.session.add(location)
        db.session.commit()

        return redirect(url_for('location.browse'))
    return render_template('location/location_profile.html',
                           location=location,
                           form=form)


@mod_location.route('/view', methods=['GET', 'POST'])
@mod_location.route('/view/<int:location_id>/view',
                    methods=['GET', 'POST'])
def location_view(location_id=None):
    #location = Location.query.get(location_id)
    form = LocationProfileForm(obj=location)

    form.kind_id.choices = [(k.id, k.name) for k in LocationKind.query.all()]

    if request.method == 'POST':
        form = LocationProfileForm(request.form)
        form.populate_obj(location)
        db.session.add(location)
        db.session.commit()

        return redirect(url_for('location.browse'))
    return render_template('location/location_profile.html',
                           location=location,
                           form=form)



@mod_location.route('/profile/dashboard', methods=['GET'])
@mod_location.route('/profile/<int:location_id>/dashboard', methods=['GET'])
def location_dashboard(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_dashboard.html',
                           location=location)


@mod_location.route('/profile/communication', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/communication',
                    methods=['GET'])
def location_communication(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_communication.html',
                           location=location)


@mod_location.route('/profile/documents', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/documents', methods=['GET'])
def location_documents(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_documents.html',
                           location=location)


@mod_location.route('/profile/builds', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/builds', methods=['GET'])
def location_builds(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_builds.html',
                           location=location)


@mod_location.route('/profile/requirements', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/requirements', methods=['GET'])
def location_requirements(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_requirements.html',
                           location=location)


@mod_location.route('/profile/customers', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/customers', methods=['GET'])
def location_customers(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_customers.html',
                           location=location)


@mod_location.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/test-cases', methods=['GET'])
def location_test_cases(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_test_cases.html',
                           location=location)


@mod_location.route('/profile/locations', methods=['GET', 'POST'])
@mod_location.route('/profile/<int:location_id>/locations', methods=['GET'])
def location_locations(location_id=None):
    if location_id:
        location = Location.query.get(location_id)
    else:
        location = None
    return render_template('location/location_single_locations.html',
                           location=location)
